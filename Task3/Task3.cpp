#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
/*
�������� 3
ϳ��������� y=x^N �� ������� ����������
*/
int pow(int x, int n) {
	if (n == 1)
		return x;
	if (n % 2 == 0) {
		int t = pow(x,n / 2);
		return t * t;
	}
	else {
		int t = pow(x, n - 1);
		return t * x;
	}
}
int main() {
	int x, n;
	printf("Enter x and n:\n");
	scanf("%d%d", &x, &n);
	printf("%d", pow(x,n));
	return 0;
}
