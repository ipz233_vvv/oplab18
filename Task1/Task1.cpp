#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
/*
�������� 1.
�� ��������� ���������� ������� �������� 
����� ����������� �������� ������������ ������
*/
int findmin(int m[],int i,int n) {
	if (i == n - 1)
		return m[i];
	int prev = findmin(m, i + 1, n);
	return prev < m[i] ? prev : m[i];
}
int main() {
	srand(time(NULL));
	int m[10000];
	int n;
	printf("Enter array size:\n");
	scanf("%d", &n);
	for (int i = 0; i < n; i++) {
		m[i] = rand();
		printf("%d ", m[i]);
	}
	printf("\n%d", findmin(m, 0, n));
	return 0;
}
